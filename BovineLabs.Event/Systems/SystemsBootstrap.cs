using BovineLabs.Event.Systems;
using Unity.Entities;

public static class SystemsBootstrap
{
    public static void AddSystems(World world)
    {
        var eventSystem = world.CreateSystemManaged<EventSystem>();
        world.GetOrCreateSystemManaged<LateSimulationSystemGroup>().AddSystemToUpdateList(eventSystem);

        var fixedStepEventSystem = world.CreateSystemManaged<FixedStepEventSystem>();
        world.GetOrCreateSystemManaged<FixedStepSimulationSystemGroup>().AddSystemToUpdateList(fixedStepEventSystem);
    }
}